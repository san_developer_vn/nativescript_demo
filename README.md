# NativeScript TypeScript Template

This template creates a NativeScript app with the NativeScript hello world example,
however, in this template the example is built with TypeScript.

You can create a new app that uses this template with either the `--template` option.

```
tns create my-app-name --template tns-template-hello-world-ts
```

Or the `--tsc` shorthand.

```
tns create my-app-name --tsc
```

> Note: Both commands will create a new NativeScript app that uses the latest version of this template published to [npm] (https://www.npmjs.com/package/tns-template-hello-world-ts).

If you want to create a new app that uses the source of the template from the `master` branch, you can execute the following:

```
tns create my-app-name --template https://github.com/NativeScript/template-hello-world-ts.git#master
```

# Setup

Using command line to add ios and android platform, list all connected devices and make a build, deploy on simulator or run on debug mode

The following command only focus on ios platform. If you need to run on android, you must replace `ios` by `android`

Using `tns --help` or `tns <command> --help` to get more help.

```
tns platform add ios
tns prepare ios
tns build ios
tns device
tns debug ios --inspector `to using safari, if use the default it will launch with Chrome DevTools`
```

# Components

The markup in NativeScript apps contains a series of user interface components, each
of which NativeScript renders with a platform-specific iOS or Android native control.
You can find a full list of user interface components you can use in your app at
[UI Components](https://docs.nativescript.org/ui/components).

The ActionBar is the NativeScript common abstraction over the Android ActionBar and iOS NavigationBar. [UI Action-Bar](http://docs.nativescript.org/ui/action-bar)

# Miscellaneous

# Integrate Google Signin and Facebook Signin

Using nativescript-social-login plugin. [document](https://github.com/mkloubert/nativescript-social-login)

## On Android

Using command line to generate keystore: password for this key is `admin1234`
```
keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000
```
Configure client on google developer [Gogole SignIn on Android](https://developers.google.com/identity/sign-in/android/start-integrating)

client ID: 830891268384-dl8d592ukldhs8e8n2namt1eqv7p4m3r.apps.googleusercontent.com

[Facebook account setup](https://developers.facebook.com/docs/)
App ID: 2150710681853522

## On iOS

Link reference [document](https://developers.google.com/identity/sign-in/ios/start-integrating)

client ID: 830891268384-9tkdriqundafkt3gnuv29gqk9al6iq89.apps.googleusercontent.com