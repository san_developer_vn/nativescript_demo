import * as httpModule from 'http';
import { extend } from 'underscore';

export interface IDataIntegrate {
    getAll(): Promise<any>;
    getBy(params: any): Promise<any>;
    save(target: any): Promise<any>;
    create(target: any): Promise<any>;
    remove(id: string): Promise<any>;
}

export default class HttpService implements IDataIntegrate {
    private _baseUrl = 'https://private-8d861-konyweather.apiary-mock.com';
    private _defaultConfig = {
        headers: { 
            "Content-Type": "application/json" 
        }
    };
    protected url: string;
    protected tag: string = 'HttpService';

    constructor(url: string) {
        this.url = url;
    };

    getAll(config?: httpModule.HttpRequestOptions): Promise<any> {
        console.log(`invoke getAll method from ${this.tag}`);

        return new Promise((resolve, reject) => {
            let requestConfig = (extend({}, this._defaultConfig, config || {}) as httpModule.HttpRequestOptions);
            requestConfig.url = this._baseUrl + this.url;
            requestConfig.method = 'GET';

            httpModule.request(requestConfig).then(
                (response: httpModule.HttpResponse) => {
                    this.handleResponse(response, resolve, reject);
                }
            ).catch(
                (errors) => {
                    this.handleError(errors);
                    reject();
                }
            );
        });
    }
    getBy(params: any, config?: httpModule.HttpRequestOptions): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestConfig = (extend({}, this._defaultConfig, config || {}) as httpModule.HttpRequestOptions);
            requestConfig.url = this._baseUrl + this.url;
            requestConfig.method = 'GET';
            requestConfig.content = JSON.stringify(params);

            httpModule.request(requestConfig).then(
                (response: httpModule.HttpResponse) => {
                    this.handleResponse(response, resolve, reject);
                }
            ).catch(
                (errors) => {
                    this.handleError(errors);
                    reject();
                }
            );
        });
    }
    save(target: any, config?: httpModule.HttpRequestOptions): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestConfig = (extend({}, this._defaultConfig, config || {}) as httpModule.HttpRequestOptions);
            requestConfig.url = this._baseUrl + this.url;
            requestConfig.method = 'PUT';
            requestConfig.content = JSON.stringify(target);

            httpModule.request(requestConfig).then(
                (response: httpModule.HttpResponse) => {
                    this.handleResponse(response, resolve, reject);
                }
            ).catch(
                (errors) => {
                    this.handleError(errors);
                    reject();
                }
            );
        });
    }
    create(target: any, config?: httpModule.HttpRequestOptions): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestConfig = (extend({}, this._defaultConfig, config || {}) as httpModule.HttpRequestOptions);
            requestConfig.url = this._baseUrl + this.url;
            requestConfig.method = 'POST';
            requestConfig.content = JSON.stringify(target);

            httpModule.request(requestConfig).then(
                (response: httpModule.HttpResponse) => {
                    this.handleResponse(response, resolve, reject);
                }
            ).catch(
                (errors) => {
                    this.handleError(errors);
                    reject();
                }
            );
        });
    }
    remove(id: string, config?: httpModule.HttpRequestOptions): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestConfig = (extend({}, this._defaultConfig, config || {}) as httpModule.HttpRequestOptions);
            requestConfig.url = this._baseUrl + this.url;
            requestConfig.method = 'DELETE';
            requestConfig.content = JSON.stringify({id: id});

            httpModule.request(requestConfig).then(
                (response: httpModule.HttpResponse) => {
                    this.handleResponse(response, resolve, reject);
                }
            ).catch(
                (errors) => {
                    this.handleError(errors);
                    reject();
                }
            );
        });
    }
    request(url: string, method: string, data?: object, config?: httpModule.HttpRequestOptions): Promise<any> {
        return new Promise((resolve, reject) => {
            let requestConfig = (extend({}, this._defaultConfig, config || {}) as httpModule.HttpRequestOptions);
            requestConfig.url = this._baseUrl + url;
            requestConfig.method = method;
            
            if (data) {
                requestConfig.content = JSON.stringify(data);
            }

            httpModule.request(requestConfig).then(
                (response: httpModule.HttpResponse) => {
                    this.handleResponse(response, resolve, reject);
                }
            ).catch(
                (errors) => {
                    this.handleError(errors);
                    reject();
                }
            );
        });
    }
    protected handleError(err: any) {
        console.log(`There is an error in component ${this.tag}. The content detail is list below`);
        console.log(err);
    }
    protected handleResponse(response: httpModule.HttpResponse, resolve: Function, reject: Function) {
        switch (response.statusCode) {
            case 200:
                resolve(response.content.toJSON());
                break;
            default:
                console.log('reject at httpService');
                reject();
        }
    }
}
