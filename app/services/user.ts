import httpService from './http';

export default class UserService extends httpService {
    constructor () {
        super('/users');
        this.tag = 'UserService';
    }

    login(username: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.request('/login', 'POST', {
                username: username,
                password: password
            }).then(
                (data) => {
                    resolve(data);
                }, 
                () => {
                    console.log('user service reject');
                    reject();
                }
            );
        });
    }
    handleError(err: any) {
        super.handleError(err);
        console.log('Additional comment');
    }
}