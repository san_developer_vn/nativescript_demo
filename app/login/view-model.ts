import { Observable } from 'data/observable';
import UserService from '~/services/user';
import User from '~/custom-type/user';
import { getFrameById } from 'tns-core-modules/ui/frame';

export class LoginViewModel extends Observable {
    username: string;
    password: string;
    user: User;
    private userService: UserService;

    constructor() {
        super();
        this.userService = new UserService();
    }

    login() {
        //Show loading indicator
        console.log('invoke login method of loginViewModel');
        this.userService.login(this.username, this.password).then(
            (data) => {
                // hide loading indicator
                console.log(data);
                this.user = new User(data);
                const frame = getFrameById('defaultFrame');
                frame.navigate({
                    moduleName: 'contactPage',
                    context: {
                        user: this.user
                    }
                });
            },
            () => {
                console.log('reject');
                // hide loading indicator
            }
        );
    }
}