import * as application from 'application';
import { ContactName } from 'nativescript-contacts';

application.getResources().contactDisplayName = (value: ContactName) => {
    return value.displayname || `${value.family || ''} ${value.given || ''} ${value.middle || ''}`;
};