import * as moment from 'moment';

interface IUser {
    id?: string;
    firstName: string;
    lastName: string;
    dob: Date;
    [propName: string]: any; // Excess Property check
}

export default class User {
    id: string;
    firstName: string;
    lastName: string;
    private _dob: Date;

    set dob(localDate: string) {
        this._dob = moment(localDate).utc().toDate();
    }
    get dob(): string {
        return moment(this._dob).toLocaleString();
    }
    
    get fullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
    
    constructor(data: IUser) {
        this.id = data.id || '';
        this.firstName = data.firstName;
        this.lastName = data.lastName;
        this._dob = data.dob;
    };
};
