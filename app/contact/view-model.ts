import { Observable, EventData } from 'data/observable';
import { getAllContacts, Contact, ContactField, KnownLabel } from 'nativescript-contacts';
import { MobilePhoneCodes, PhoneNumberCode, DigitalNumberCodes } from '~/phone-number/service';

export class ContactViewModel extends Observable{
    private _contacts: Array<Contact>;

    constructor() {
        super();
        this._contacts = new Array<Contact>();
    }

    get contacts() {
        return this._contacts;
    }

    set contacts(value: Array<Contact>) {
        this._contacts = value;
        this.notifyPropertyChange('contacts', value);
    }

    public onGetAll() {
        let targetContacts = new Array<Contact>();
        const contactFields = ['name', 'phoneNumbers'];

        getAllContacts(contactFields).then((args) => {
            args.data.forEach((contactItem) => {
                for (let i = 0, len = contactItem.phoneNumbers.length; i < len; i++) {
                    const phone = contactItem.phoneNumbers[i];
                    
                    if (this.convertMobilePhoneCode(phone.value) || this.convertDigitalPhoneCode(phone.value) || this.isNewFormatPhoneNumber(phone.value)) {
                        targetContacts.push(contactItem);
                        break;
                    }
                }
            });

            this.contacts = targetContacts;
        });
    }

    public changeAllPhoneCodes(targets: Array<String>) {
        this.contacts.forEach((contact) => {
            if (targets.indexOf(contact.id) < 0) {
                return;
            }

            let isMustModify = false;
            let validPhoneNumbers: Array<ContactField> = [];

            contact.phoneNumbers.forEach((phoneNumber, index) => {
                let newPhoneCode = this.convertDigitalPhoneCode(phoneNumber.value) || this.convertMobilePhoneCode(phoneNumber.value);
                
                if (newPhoneCode) {
                    isMustModify = true;

                    if (!this.isNewFormatPhoneNumber(phoneNumber.value)) {
                        phoneNumber.value = newPhoneCode;
                    } else {
                        let newPhoneNumber = this.createPhoneNumber(newPhoneCode, phoneNumber.label);
                        
                        if (!this.isExistedPhoneNumber(validPhoneNumbers, newPhoneNumber.value)) {
                            validPhoneNumbers.push(newPhoneNumber);
                        } 
                    }
                }

                if (!this.isExistedPhoneNumber(validPhoneNumbers, phoneNumber.value)) {
                    validPhoneNumbers.push(phoneNumber);
                } 
            });

            if (isMustModify) {
                contact.phoneNumbers = validPhoneNumbers;
                try {
                    contact.save();
                } catch (error) {
                    console.log(error);
                }
            }
        });

        
    }

    public changeDigitalCode(targets: Array<String>) {
        this.contacts.forEach((contact) => {
            if (targets.indexOf(contact.id) < 0) {
                return;
            }

            let isMustModify = false;
            let validPhoneNumbers: Array<ContactField> = [];

            contact.phoneNumbers.forEach((phoneNumber, index) => {
                let newPhoneCode = this.convertDigitalPhoneCode(phoneNumber.value);
                
                if (newPhoneCode) {
                    isMustModify = true;

                    if (!this.isNewFormatPhoneNumber(phoneNumber.value)) {
                        phoneNumber.value = newPhoneCode;
                    } else {
                        let newPhoneNumber = this.createPhoneNumber(newPhoneCode, phoneNumber.label);
                        
                        if (!this.isExistedPhoneNumber(validPhoneNumbers, newPhoneNumber.value)) {
                            validPhoneNumbers.push(newPhoneNumber);
                        } 
                    }
                }

                if (!this.isExistedPhoneNumber(validPhoneNumbers, phoneNumber.value)) {
                    validPhoneNumbers.push(phoneNumber);
                } 
            });

            if (isMustModify) {
                contact.phoneNumbers = validPhoneNumbers;
                try {
                    contact.save();
                } catch (error) {
                    console.log(error);
                }
            }
        });
    }

    public changeMobileCode(targets: Array<String>) {
        this.contacts.forEach((contact) => {
            if (targets.indexOf(contact.id) < 0) {
                return;
            }
            let isMustModify = false;
            let validPhoneNumbers: Array<ContactField> = [];

            contact.phoneNumbers.forEach((phoneNumber, index) => {
                let newPhoneCode = this.convertMobilePhoneCode(phoneNumber.value);
                
                if (newPhoneCode) {
                    isMustModify = true;
                    phoneNumber.value = newPhoneCode;
                }

                if (!this.isExistedPhoneNumber(validPhoneNumbers, phoneNumber.value)) {
                    validPhoneNumbers.push(phoneNumber);
                } 
            });

            if (isMustModify) {
                contact.phoneNumbers = validPhoneNumbers;
                try {
                    contact.save();
                } catch (error) {
                    console.log(error);
                }
            }
        });
    }

    private convertMobilePhoneCode (phoneNumber: string): string {
        let item = phoneNumber;
        let isConvertedCode = false;

        item = item.replace(/\+84/, '0').match(/\d/g).join('');

        if (item.length > 10) {
            for (let i = 0, len = MobilePhoneCodes.length; i < len; i++) {
                let provider: PhoneNumberCode = MobilePhoneCodes[i];
    
                if (item.substr(0, 4) == provider.oldIdentifiedNumber) {
                    item = item.replace(/\d{4}/, provider.newIdentifiedNumber);
                    isConvertedCode = true;
                    break;
                }
            }
        }

        if (!isConvertedCode) {
            item = null;
        }

        return item;
    }

    private convertDigitalPhoneCode(phoneNumber: string): string {
        let item = phoneNumber;
        let isConvertedCode = false;

        item = item.replace(/\+84/, '0').match(/\d/g).join('');

        if (item.length >= 10) {
            for (let j = 0, digitalLen = DigitalNumberCodes.length; j < digitalLen; j++) {
                let digitalPhone:PhoneNumberCode = DigitalNumberCodes[j];
                let digitalReg = new RegExp(`^(0${digitalPhone.oldIdentifiedNumber})(\\d{7})$`);

                if (digitalReg.test(item)) {
                    item = item.replace(digitalReg, (val, $1, $2) => { 
                        return `0${digitalPhone.newIdentifiedNumber}${$2}`
                    });
                    isConvertedCode = true;
                    break;
                }
            };
        }

        if (!isConvertedCode) {
            item = null;
        }

        return item;
    }

    private isNewFormatPhoneNumber (phoneNumber: string): boolean {
        let isNewFormat = false;

        if (phoneNumber.length == 10) {
            for (let i = 0, len = MobilePhoneCodes.length; i < len; i++) {
                let provider: PhoneNumberCode = MobilePhoneCodes[i];
    
                if (phoneNumber.substr(0, 3) == provider.newIdentifiedNumber) {
                    isNewFormat = true;
                    break;
                }
            }
        }

        return isNewFormat;
    }

    private createPhoneNumber (phoneNumber: string, originlabel: string): ContactField {
        if (!phoneNumber) {
            return;
        };

        let newPhoneNumber: ContactField = null;

        newPhoneNumber = {
            label: originlabel || KnownLabel.OTHER,
            value: phoneNumber
        };
    
        return newPhoneNumber;
    }

    private isExistedPhoneNumber (originPhoneNumbers: Array<ContactField>, newPhoneNumber: string): boolean {
        let isExisted = false;
        
        for (let i = 0, len = originPhoneNumbers.length; i < len; i++) {
            if (newPhoneNumber == originPhoneNumbers[i].value) {
                isExisted = true;
                break;
            }
        }
        return isExisted;
    }
}