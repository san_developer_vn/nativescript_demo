/// <reference path="../../node_modules/tns-platform-declarations/android.d.ts"/>

import * as application from 'application';
import { EventData, Observable, PropertyChangeData } from 'data/observable';
import { Page } from 'ui/page';
import { ContactViewModel } from './view-model';
import { requestPermissions } from 'nativescript-permissions';
import * as platform from 'platform';
import { ItemEventData, ListView } from 'ui/list-view'
import '~/filters/common';

let page: Page;
let _selectAll = false;
let vm = new ContactViewModel();
let seletectedContacts = new Array<String>();

export function navigatingTo (args: EventData) {
    // application.setCssFileName("skins/dark/index.css"); // Can be use to change theme
    page = <Page>args.object;
    vm.set('selectAll', _selectAll);
    updateTitle();

    if (platform.isAndroid) {
        requestPermissions([android.Manifest.permission.READ_CONTACTS, android.Manifest.permission.WRITE_CONTACTS], 'Need to access your contact').then(() => {
            vm.onGetAll();
            page.bindingContext = vm;
        });
    } else {    
        vm.onGetAll();
        page.bindingContext = vm;
    }

    // Add property listener
    vm.addEventListener(Observable.propertyChangeEvent, (args: PropertyChangeData) => {
        switch (args.propertyName) {
            case 'selectAll':
                if (args.value === null) {
                    _selectAll = false;
                    break;
                }

                if (args.value) {
                    seletectedContacts = vm.contacts.map((item) => { return item.id; });
                    updateItemCls(page, true);
                } else {
                    seletectedContacts = [];
                    updateItemCls(page);
                }

                vm.set('seletectedContacts', seletectedContacts);
                _selectAll = args.value;
                break;
            case 'seletectedContacts':
                updateTitle();
                break;
        }
    })
}

export function goSetting () {
    page.frame.navigate('phone-number/page');
}

export function changeMobileCode() {
    vm.changeMobileCode(seletectedContacts);
    seletectedContacts = [];
    updateItemCls(page);
    refreshListView(page);
}

export function changeDigitalCode() {
    vm.changeDigitalCode(seletectedContacts);
    seletectedContacts = [];
    updateItemCls(page);
    refreshListView(page);
}

export function changeAllPhoneCodes() {
    vm.changeAllPhoneCodes(seletectedContacts);
    seletectedContacts = [];
    updateItemCls(page);
    refreshListView(page);
}

export function onItemTap(args: ItemEventData) {
    const item = vm.contacts[args.index];
    const indexOf = seletectedContacts.indexOf(item.id);

    if (indexOf < 0) {
        seletectedContacts.push(item.id);
        args.view.className = 'list-group-item item-selected';
    } else {
        seletectedContacts.splice(indexOf, 1);
        args.view.className = 'list-group-item';
    }

    if (_selectAll) {
        vm.set('selectAll', null);
        vm.notifyPropertyChange('seletectedContacts', seletectedContacts);
    } else {
        if (vm.contacts.length === seletectedContacts.length) {
            vm.set('selectAll', true);
        } else {
            vm.notifyPropertyChange('seletectedContacts', seletectedContacts);
        }
    }
}

function refreshListView(page: Page) {
    const contactListView = <ListView> page.getViewById('contactListView');
    contactListView.refresh();
}

function updateItemCls(page: Page, isCheckedAll: boolean = false) {
    const contactListView = <ListView> page.getViewById('contactListView');
    contactListView.eachChildView((view) => {
        view.className = isCheckedAll ? 'list-group-item item-selected' : 'list-group-item'
        return true;
    });
}

function updateTitle() {
    const message = `Bạn đã chọn ${seletectedContacts.length} danh bạ để thực hiện thay đổi`;
    vm.set('title', message);
}