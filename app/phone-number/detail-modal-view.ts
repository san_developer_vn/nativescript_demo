import { fromObject } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
let closeCallback;

export function onShownModally(args) {
    const context = args.context;
    closeCallback = args.closeCallback;
    const page: Page = <Page>args.object;

    console.log(context);

    page.bindingContext = fromObject(context);
}

export function onSaveButtonTap(args) {
    const page: Page = <Page>args.object.page;
    const bindingContext = page.bindingContext;
    const oldIdentifiedNumber = bindingContext.get("oldIdentifiedNumber");
    const newIdentifiedNumber = bindingContext.get("newIdentifiedNumber");
    closeCallback(oldIdentifiedNumber, newIdentifiedNumber);
}