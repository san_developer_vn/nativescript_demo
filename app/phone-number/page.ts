import { EventData, fromObject, Observable, PropertyChangeData } from 'data/observable';
import { Page, View } from 'ui/page';
import { topmost } from 'ui/frame';
import { MobilePhoneCodes, DigitalNumberCodes } from './service';
import { ItemEventData } from 'tns-core-modules/ui/list-view/list-view';

let viewModel = fromObject({
    activePhoneCodes: MobilePhoneCodes,
    isMobileCode: true,
    selectedBarIndex: 0
});

const detailModalPage = 'phone-number/detail-modal-view';

export function onNavigatedTo (args: EventData) {
    let page = <Page>args.object;
    
    viewModel.addEventListener(Observable.propertyChangeEvent, (args: PropertyChangeData) => {
		if (args.propertyName === "selectedBarIndex") {
			const selectedPhoneCode = args.value === 0 ? MobilePhoneCodes : DigitalNumberCodes
			viewModel.set("activePhoneCodes", selectedPhoneCode);
		}
    });
    
    page.bindingContext = viewModel;
}

export function onNavBtnTap() {
    topmost().goBack();
}

export function onItemTap(args: ItemEventData) {
    const view = <View> args.view;
    const context = view.bindingContext;
    const fullscreen = true;
    
    console.log(context);

    view.showModal(detailModalPage, context, (oldIdentifiedNumber, newIdentifiedNumber) => {
        alert(`this is callback data ${oldIdentifiedNumber} ${newIdentifiedNumber}`);
    }, fullscreen);
}