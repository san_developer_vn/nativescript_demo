
// Reference link http://vietnamnet.vn/vn/cong-nghe/vien-thong/cach-goi-cho-thue-bao-11-so-sau-khi-chuyen-sang-10-so-cua-cac-nha-mang-453985.html

export interface PhoneNumberCode {
    name: string;
    oldIdentifiedNumber: string;
    newIdentifiedNumber: string;
}

export let MobilePhoneCodes: Array<PhoneNumberCode> = [
    {
        name: 'MobiFone',
        oldIdentifiedNumber: '0120',
        newIdentifiedNumber: '070'
    },
    {
        name: 'MobiFone',
        oldIdentifiedNumber: '0121',
        newIdentifiedNumber: '079'
    },
    {
        name: 'MobiFone',
        oldIdentifiedNumber: '0122',
        newIdentifiedNumber: '077'
    },
    {
        name: 'MobiFone',
        oldIdentifiedNumber: '0126',
        newIdentifiedNumber: '076'
    },
    {
        name: 'MobiFone',
        oldIdentifiedNumber: '0128',
        newIdentifiedNumber: '078'
    },
    {
        name: 'VinaPhone',
        oldIdentifiedNumber: '0123',
        newIdentifiedNumber: '083'
    },
    {
        name: 'VinaPhone',
        oldIdentifiedNumber: '0124',
        newIdentifiedNumber: '084'
    },
    {
        name: 'VinaPhone',
        oldIdentifiedNumber: '0125',
        newIdentifiedNumber: '085'
    },
    {
        name: 'VinaPhone',
        oldIdentifiedNumber: '0127',
        newIdentifiedNumber: '081'
    },
    {
        name: 'VinaPhone',
        oldIdentifiedNumber: '0129',
        newIdentifiedNumber: '082'
    },
    {
        name: 'Viettel',
        oldIdentifiedNumber: '0162',
        newIdentifiedNumber: '032'
    },
    {
        name: 'Viettel',
        oldIdentifiedNumber: '0163',
        newIdentifiedNumber: '033'
    },
    {
        name: 'Viettel',
        oldIdentifiedNumber: '0164',
        newIdentifiedNumber: '034'
    },
    {
        name: 'Viettel',
        oldIdentifiedNumber: '0165',
        newIdentifiedNumber: '035'
    },
    {
        name: 'Viettel',
        oldIdentifiedNumber: '0166',
        newIdentifiedNumber: '036'
    },
    {
        name: 'Viettel',
        oldIdentifiedNumber: '0167',
        newIdentifiedNumber: '037'
    },
    {
        name: 'Viettel',
        oldIdentifiedNumber: '0168',
        newIdentifiedNumber: '038'
    },
    {
        name: 'Vietnamobile',
        oldIdentifiedNumber: '0186',
        newIdentifiedNumber: '056'
    },
    {
        name: 'Vietnamobile',
        oldIdentifiedNumber: '0188',
        newIdentifiedNumber: '058'
    },
    {
        name: 'Gmobile',
        oldIdentifiedNumber: '0199',
        newIdentifiedNumber: '059'
    }
];

export let DigitalNumberCodes: Array<PhoneNumberCode> = [
    { name: 'Sơn La', oldIdentifiedNumber: '22', newIdentifiedNumber: '212' },
    { name: 'Lai Châu', oldIdentifiedNumber: '231', newIdentifiedNumber: '213' },
    { name: 'Lào Cai', oldIdentifiedNumber: '20', newIdentifiedNumber: '214' },
    { name: 'Điện Biên', oldIdentifiedNumber: '230', newIdentifiedNumber: '215' },
    { name: 'Yên Bái', oldIdentifiedNumber: '29', newIdentifiedNumber: '216' },
    { name: 'Quảng Bình', oldIdentifiedNumber: '52', newIdentifiedNumber: '232' },
    { name: 'Quảng Trị', oldIdentifiedNumber: '53', newIdentifiedNumber: '233' },
    { name: 'Thừa Thiên - Huế', oldIdentifiedNumber: '54', newIdentifiedNumber: '234' },
    { name: 'Quảng Nam', oldIdentifiedNumber: '510', newIdentifiedNumber: '235' },
    { name: 'Đà Nẵng', oldIdentifiedNumber: '511', newIdentifiedNumber: '236' },
    { name: 'Thanh Hoá', oldIdentifiedNumber: '37', newIdentifiedNumber: '237' },
    { name: 'Nghệ An', oldIdentifiedNumber: '38', newIdentifiedNumber: '238' },
    { name: 'Hà Tĩnh', oldIdentifiedNumber: '39', newIdentifiedNumber: '239' },
    { name: 'Quảng Ninh', oldIdentifiedNumber: '33', newIdentifiedNumber: '203' },
    { name: 'Bắc Giang', oldIdentifiedNumber: '240', newIdentifiedNumber: '204' },
    { name: 'Lạng Sơn', oldIdentifiedNumber: '25', newIdentifiedNumber: '205' },
    { name: 'Cao Bằng', oldIdentifiedNumber: '26', newIdentifiedNumber: '206' },
    { name: 'Tuyên Quang', oldIdentifiedNumber: '27', newIdentifiedNumber: '207' },
    { name: 'Thái Nguyên ', oldIdentifiedNumber: '280', newIdentifiedNumber: '208' },
    { name: 'Bắc Cạn', oldIdentifiedNumber: '281', newIdentifiedNumber: '209' },
    { name: 'Hải Dương', oldIdentifiedNumber: '320', newIdentifiedNumber: '220' },
    { name: 'Hưng Yên', oldIdentifiedNumber: '321', newIdentifiedNumber: '221' },
    { name: 'Bắc Ninh', oldIdentifiedNumber: '241', newIdentifiedNumber: '222' },
    { name: 'Hải Phòng', oldIdentifiedNumber: '31', newIdentifiedNumber: '225' },
    { name: 'Hà Nam', oldIdentifiedNumber: '351', newIdentifiedNumber: '226' },
    { name: 'Thái Bình', oldIdentifiedNumber: '36', newIdentifiedNumber: '227' },
    { name: 'Nam Định', oldIdentifiedNumber: '350', newIdentifiedNumber: '228' },
    { name: 'Ninh Bình', oldIdentifiedNumber: '30', newIdentifiedNumber: '229' },
    { name: 'Cà Mau', oldIdentifiedNumber: '780', newIdentifiedNumber: '290' },
    { name: 'Bạc Liêu', oldIdentifiedNumber: '781', newIdentifiedNumber: '291' },
    { name: 'Cần Thơ ', oldIdentifiedNumber: '710', newIdentifiedNumber: '292' },
    { name: 'Hậu Giang', oldIdentifiedNumber: '711', newIdentifiedNumber: '293' },
    { name: 'Trà Vinh', oldIdentifiedNumber: '74', newIdentifiedNumber: '294' },
    { name: 'An Giang', oldIdentifiedNumber: '76', newIdentifiedNumber: '296 ' },
    { name: 'Kiên Giang', oldIdentifiedNumber: '77', newIdentifiedNumber: '297' },
    { name: 'Sóc Trăng', oldIdentifiedNumber: '79', newIdentifiedNumber: '299' },
    { name: 'Hà Nội', oldIdentifiedNumber: '4', newIdentifiedNumber: '24' },
    { name: 'Hồ Chí Minh', oldIdentifiedNumber: '8', newIdentifiedNumber: '28' },
    { name: 'Đồng Nai', oldIdentifiedNumber: '61', newIdentifiedNumber: '251' },
    { name: 'Bình Thuận', oldIdentifiedNumber: '62', newIdentifiedNumber: '252' },
    { name: 'Bà Rịa - Vũng Tàu', oldIdentifiedNumber: '64', newIdentifiedNumber: '254' },
    { name: 'Quảng Ngãi', oldIdentifiedNumber: '55', newIdentifiedNumber: '255' },
    { name: 'Bình Định', oldIdentifiedNumber: '56', newIdentifiedNumber: '256' },
    { name: 'Phú Yên', oldIdentifiedNumber: '57', newIdentifiedNumber: '257' },
    { name: 'Khánh Hoà', oldIdentifiedNumber: '58', newIdentifiedNumber: '258' },
    { name: 'Ninh Thuận', oldIdentifiedNumber: '68', newIdentifiedNumber: '259' },
    { name: 'Kon Tum', oldIdentifiedNumber: '60', newIdentifiedNumber: '260' },
    { name: 'Đắk Nông', oldIdentifiedNumber: '501', newIdentifiedNumber: '261' },
    { name: 'Đắk Lắk', oldIdentifiedNumber: '500', newIdentifiedNumber: '262' },
    { name: 'Lâm Đồng', oldIdentifiedNumber: '63', newIdentifiedNumber: '263' },
    { name: 'Gia Lai', oldIdentifiedNumber: '59', newIdentifiedNumber: '269' },
    { name: 'Vĩnh Long', oldIdentifiedNumber: '70', newIdentifiedNumber: '270' },
    { name: 'Bình Phước', oldIdentifiedNumber: '651', newIdentifiedNumber: '271' },
    { name: 'Long An', oldIdentifiedNumber: '72', newIdentifiedNumber: '272' },
    { name: 'Tiền Giang', oldIdentifiedNumber: '73', newIdentifiedNumber: '273' },
    { name: 'Bình Dương', oldIdentifiedNumber: '650', newIdentifiedNumber: '274' },
    { name: 'Bến Tre', oldIdentifiedNumber: '75', newIdentifiedNumber: '275' },
    { name: 'Tây Ninh', oldIdentifiedNumber: '66', newIdentifiedNumber: '276' },
    { name: 'Đồng Tháp', oldIdentifiedNumber: '67', newIdentifiedNumber: '277' }
]